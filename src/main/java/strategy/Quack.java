package strategy;

/**
 * Created by Emil on 21/11/2016.
 */
public class Quack implements QuackBehavior {
    public void quack() {
        System.out.println("Quack");
    }
}
