package strategy;

/**
 * Created by Emil on 21/11/2016.
 */
public class MallardDuck extends Duck {
    public MallardDuck() {
        quackBehavior = new Quack();
        flyBehavior = new FlyWithWings();
    }

    void display() {
        System.out.println("I'm real Mallard duck");
    }
}
