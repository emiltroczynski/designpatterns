package strategy;

/**
 * Created by Emil on 21/11/2016.
 */
public interface FlyBehavior {
    public void fly();
}
