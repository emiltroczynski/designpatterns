package strategy;

/**
 * Created by Emil on 21/11/2016.
 */
public class ModelDuck extends Duck {
    public ModelDuck() {
        flyBehavior = new FlyNoWay();
        quackBehavior = new Quack();
    }

    void display() {
        System.out.println("I'm model duck");
    }
}
