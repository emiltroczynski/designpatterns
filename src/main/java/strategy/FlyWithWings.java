package strategy;

/**
 * Created by Emil on 21/11/2016.
 */
public class FlyWithWings implements FlyBehavior {
    public void fly() {
        System.out.println("I'm flying!!");
    }
}
