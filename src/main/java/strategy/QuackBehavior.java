package strategy;

/**
 * Created by Emil on 21/11/2016.
 */
public interface QuackBehavior {
    public void quack();
}
