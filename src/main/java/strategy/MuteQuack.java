package strategy;

/**
 * Created by Emil on 21/11/2016.
 */
public class MuteQuack implements QuackBehavior {
    public void quack() {
        System.out.println("<< Silence >>");
    }
}
