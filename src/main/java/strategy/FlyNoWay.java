package strategy;

/**
 * Created by Emil on 21/11/2016.
 */
public class FlyNoWay implements FlyBehavior {
    public void fly() {
        System.out.println("I can't fly");
    }
}
