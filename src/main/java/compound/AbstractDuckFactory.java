package compound;

/**
 * Created by Emil_Troczynski on 01/01/2017.
 */
public abstract class AbstractDuckFactory {
    public abstract Quackable createMallardDuck();
    public abstract Quackable createReadheadDuck();
    public abstract Quackable createDuckCall();
    public abstract Quackable createRubberDuck();
}
