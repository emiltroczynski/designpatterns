package compound;

/**
 * Created by Emil_Troczynski on 01/01/2017.
 */
public interface QuackObservable {
    public void registerObserver(Observer observer);
    public void notifyObservers();
}
