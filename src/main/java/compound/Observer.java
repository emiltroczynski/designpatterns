package compound;

/**
 * Created by Emil_Troczynski on 01/01/2017.
 */
public interface Observer {
    public void update(QuackObservable duck);
}
