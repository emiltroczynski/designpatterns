package compound;

/**
 * Created by Emil_Troczynski on 01/01/2017.
 */
public class Quackologist implements Observer {
    @Override
    public void update(QuackObservable duck) {
        System.out.println("Quackologist: " + duck + " just quacked.");
    }
}
