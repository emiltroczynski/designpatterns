package compound;

/**
 * Created by Emil_Troczynski on 01/01/2017.
 */
public interface Quackable extends QuackObservable {
    public void quack();
}
