package singleton;

/**
 * Created by Emil on 03/12/2016.
 */
public class Singleton {
    private static Singleton uniqueInstance;

    //other useful instance variables here

    private Singleton() {

    }

    public static Singleton getInstance() {
        if (uniqueInstance == null) {
            uniqueInstance = new Singleton();
        }
        return uniqueInstance;
    }

    //other useful methods here
}
