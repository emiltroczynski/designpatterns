package proxy.protection;

/**
 * Created by Emil_Troczynski on 22/12/2016.
 */
public interface PersonBean {
    String getName();

    void setName(String name);

    String getGender();

    void setGender(String gender);

    String getInterests();

    void setInterests(String interests);

    int getHotOrNotRating();

    void setHotOrNotRating(int rating);
}
