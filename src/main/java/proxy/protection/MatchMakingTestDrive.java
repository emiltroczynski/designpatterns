package proxy.protection;

import java.lang.reflect.InvocationHandler;
import java.lang.reflect.Proxy;
import java.util.HashMap;

/**
 * Created by Emil_Troczynski on 31/12/2016.
 */
public class MatchMakingTestDrive {
    HashMap<String, PersonBean> datingDB = new HashMap<String, PersonBean>();

    public MatchMakingTestDrive() {
        initializeDatabase();
    }

    public static void main(String[] args) {
        MatchMakingTestDrive test = new MatchMakingTestDrive();
        test.drive();
    }

    public void drive() {
        PersonBean joe = getPersonFromDatabase("Joe Javabean");
//        PersonBean ownerProxy = getOwnerProxy(joe);
//        PersonBean ownerProxy = getProxy(joe, new OwnerInvocationHandler(joe));
        PersonBean ownerProxy = getProxy(new OwnerInvocationHandler(joe));
        System.out.println("Name is: " + ownerProxy.getName());
        ownerProxy.setInterests("bowling, go");
        System.out.println("Interests set from owner proxy");
        try {
            ownerProxy.setHotOrNotRating(10);
        } catch (Exception e) {
            System.out.println("Can't set rating from owner proxy");
        }
        System.out.println("Rating is: " + ownerProxy.getHotOrNotRating());

//        PersonBean nonOwnerProxy = getNonOwnerProxy(joe);
//        PersonBean nonOwnerProxy = getProxy(joe, new NonOwnerInvocationHandler(joe));
        PersonBean nonOwnerProxy = getProxy(new NonOwnerInvocationHandler(joe));
        System.out.println("Name is: " + nonOwnerProxy.getName());
        try {
            nonOwnerProxy.setInterests("bowling, Go");
        } catch (Exception e) {
            System.out.println("Can't set interests from non owner proxy");
        }
        nonOwnerProxy.setHotOrNotRating(3);
        System.out.println("Rating set from non owner proxy");
        System.out.println("Rating is: " + nonOwnerProxy.getHotOrNotRating());
    }

    PersonBean getOwnerProxy(PersonBean person) {
        return (PersonBean) Proxy.newProxyInstance(
                person.getClass().getClassLoader(),
                person.getClass().getInterfaces(),
                new OwnerInvocationHandler(person)
        );
    }

    PersonBean getNonOwnerProxy(PersonBean person) {
        return (PersonBean) Proxy.newProxyInstance(
                person.getClass().getClassLoader(),
                person.getClass().getInterfaces(),
                new NonOwnerInvocationHandler(person)
        );
    }

    PersonBean getProxy(PersonBean person, InvocationHandler handler) {
        return (PersonBean) Proxy.newProxyInstance(
                person.getClass().getClassLoader(),
                person.getClass().getInterfaces(),
                handler
        );
    }

    PersonBean getProxy(InvocationHandler handler) {
        return (PersonBean) Proxy.newProxyInstance(
                new PersonBeanImpl().getClass().getClassLoader(),
                new PersonBeanImpl().getClass().getInterfaces(),
                handler
        );
    }

    private PersonBean getPersonFromDatabase(String name) {
        return (PersonBean) datingDB.get(name);
    }


    private void initializeDatabase() {
        PersonBean joe = new PersonBeanImpl();
        joe.setName("Joe Javabean");
        joe.setInterests("cars, computers, music");
        joe.setHotOrNotRating(7);
        datingDB.put(joe.getName(), joe);

        PersonBean kelly = new PersonBeanImpl();
        kelly.setName("Kelly Klosure");
        kelly.setInterests("ebay, movies, music");
        kelly.setHotOrNotRating(6);
        datingDB.put(kelly.getName(), kelly);
    }
}
