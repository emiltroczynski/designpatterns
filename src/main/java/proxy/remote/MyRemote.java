package proxy.remote;

import java.rmi.Remote;
import java.rmi.RemoteException;

/**
 * Created by Emil_Troczynski on 19/12/2016.
 */
public interface MyRemote extends Remote {
    public String sayHello() throws RemoteException;
}
