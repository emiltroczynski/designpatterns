package proxy.remote;

import java.rmi.RemoteException;

/**
 * Created by Emil_Troczynski on 18/12/2016.
 */
public class GumballMonitor {
    GumballMachineRemote machine;

    public GumballMonitor(GumballMachineRemote machine) {
        this.machine = machine;
    }

    public void report() {
        try {
            System.out.println("Gumball Machine: " + machine.getLocation());
            System.out.println("Gumball inventory: " + machine.getCount() + " gumballs");
            System.out.println("Gumball state: " + machine.getState());
        } catch (RemoteException e) {
            e.printStackTrace();
        }
    }
}
