package proxy.remote;

import java.io.Serializable;

/**
 * Created by Emil_Troczynski on 18/12/2016.
 */
public interface State extends Serializable {
    public void insertQuarter();

    public void ejectQuarter();

    public void turnCrank();

    public void dispense();
}
