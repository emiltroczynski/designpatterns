package proxy.remote;

import java.rmi.Remote;
import java.rmi.RemoteException;

/**
 * Created by Emil_Troczynski on 19/12/2016.
 */
public interface GumballMachineRemote extends Remote {
    public int getCount() throws RemoteException;

    public String getLocation() throws RemoteException;

    public String getState() throws RemoteException;
}
