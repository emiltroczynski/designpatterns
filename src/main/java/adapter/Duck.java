package adapter;

/**
 * Created by Emil_Troczynski on 12/6/2016.
 */
public interface Duck {
    public void quack();
    public void fly();
}
