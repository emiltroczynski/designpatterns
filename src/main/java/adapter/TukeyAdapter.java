package adapter;

/**
 * Created by Emil_Troczynski on 12/6/2016.
 */
public class TukeyAdapter implements Duck {
    Turkey turkey;

    public TukeyAdapter(Turkey turkey) {
        this.turkey = turkey;
    }

    @Override
    public void quack() {
        turkey.gobble();
    }

    @Override
    public void fly() {
        for (int i = 0; i < 5; i++) {
            turkey.fly();
        }
    }
}
