package adapter;

/**
 * Created by Emil_Troczynski on 12/6/2016.
 */
public interface Turkey {
    public void gobble();
    public void fly();
}
