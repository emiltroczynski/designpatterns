package factory;

import java.util.List;

/**
 * Created by Emil on 29/11/2016.
 */
public interface PizzaIngredientFactory{
    public Dough createDough();
    public Sauce createSauce();
    public Cheese createCheese();
    public List<Veggies> createVeggies();
    public Pepperoni createPepperoni();
    public Clams createClams();
}
