package factory;

/**
 * Created by Emil on 02/12/2016.
 */
public class Mozzarella implements Cheese {
    public String toString() {
        return "Mozzarella";
    }
}
