package factory;

/**
 * Created by Emil on 30/11/2016.
 */
public interface Sauce {
    public String toString();
}
