package factory;

/**
 * Created by Emil on 30/11/2016.
 */
public class ReggianoCheese implements Cheese {
    public String toString() {
        return "Reggiano Cheese";
    }
}
