package factory;

/**
 * Created by Emil on 30/11/2016.
 */
public class SlicedPepperoni implements Pepperoni {
    public String toString() {
        return "Sliced Pepperoni";
    }
}
