package factory;

/**
 * Created by Emil on 30/11/2016.
 */
public interface Cheese {
    public String toString();
}
