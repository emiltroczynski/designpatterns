package factory;

/**
 * Created by Emil on 30/11/2016.
 */
public class PlumTomatoSauce implements Sauce {
    public String toString() {
        return "Plum Tomato Sauce";
    }
}
