package factory;

/**
 * Created by Emil on 30/11/2016.
 */
public class ThickCrustDough implements Dough {
    public String toString() {
        return "Thick Dough";
    }
}
