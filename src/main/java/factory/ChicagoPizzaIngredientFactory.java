package factory;

import java.util.Arrays;
import java.util.List;

/**
 * Created by Emil on 30/11/2016.
 */
public class ChicagoPizzaIngredientFactory implements PizzaIngredientFactory {
    @Override
    public Dough createDough() {
        return new ThickCrustDough();
    }

    @Override
    public Sauce createSauce() {
        return new PlumTomatoSauce();
    }

    @Override
    public Cheese createCheese() {
        return new Mozzarella();
    }

    @Override
    public List<Veggies> createVeggies() {
        List<Veggies> veggies = Arrays.asList(new Eggplant(), new Spinach(), new BlackOlives());
        return veggies;
    }

    @Override
    public Pepperoni createPepperoni() {
        return new SlicedPepperoni();
    }

    @Override
    public Clams createClams() {
        return new FrozenClams();
    }
}
