package factory;

/**
 * Created by Emil on 30/11/2016.
 */
public class ThinCrustDough implements Dough {
    public String toString() {
        return "Thin Crust";
    }
}
