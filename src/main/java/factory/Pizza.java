package factory;

import java.util.List;

/**
 * Created by Emil on 29/11/2016.
 */
public abstract class Pizza {
    String name;

    Dough dough;
    Sauce sauce;
    List<Veggies> veggies;
    Cheese cheese;
    Pepperoni pepperoni;
    Clams clam;


    abstract void prepare();

    void bake() {
        System.out.println("Bake for 25 minutes at 350");
    }

    void cut() {
        System.out.println("Cutting the pizza into diagonal slices");
    }

    void box() {
        System.out.println("Place pizza in official PizzaStore box");
    }

    String getName() {
        return name;
    }

    void setName(String name) {
        this.name = name;
    }

    public String toString() {
        String vlist = "";
        if (veggies != null) {
            for (Veggies v : veggies) {
                vlist += v.toString() + " ";
            }
        }

        return " " + name +
                "\n\tdough: " + dough +
                "\n\tsauce: " + sauce +
                "\n\tveggies: " + vlist +
                "\n\tcheese: " + cheese +
                "\n\tpepperoni: " + pepperoni +
                "\n\tclam: " + clam
                ;
    }
}
