package factory;

/**
 * Created by Emil on 30/11/2016.
 */
public interface Pepperoni {
    public String toString();
}
