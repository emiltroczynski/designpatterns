package factory;

/**
 * Created by Emil on 30/11/2016.
 */
public class MarinSauce implements Sauce {
    public String toString() {
        return "Marinara Sauce";
    }
}
