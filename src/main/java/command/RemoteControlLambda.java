package command;

/**
 * Created by Emil on 06/12/2016.
 */
public class RemoteControlLambda {
    CommandLambda[] onCommands;
    CommandLambda[] offCommands;

    public RemoteControlLambda() {
        onCommands = new CommandLambda[7];
        offCommands = new CommandLambda[7];

        for(int i = 0; i < 7; i++) {
            onCommands[i] = () -> { };
            offCommands[i] = () -> { };
        }
    }

    public void setCommand(int slot, CommandLambda onCommand, CommandLambda offCommand) {
        onCommands[slot] = onCommand;
        offCommands[slot] = offCommand;
    }

    public void onButtonWasPushed(int slot) {
        onCommands[slot].execute();
    }

    public void offButtonWasPushed(int slot) {
        offCommands[slot].execute();
    }

    @Override
    public String toString() {
        StringBuffer stringBuff = new StringBuffer();
        stringBuff.append("\n------ Remote Control ------\n");
        for(int i =0; i < onCommands.length; i++) {
            stringBuff.append("[slot " + i + "] " + onCommands[i].getClass().getSimpleName()
                    + "\t\t" + offCommands[i].getClass().getSimpleName() + "\n");
        }
        return stringBuff.toString();
    }
}
