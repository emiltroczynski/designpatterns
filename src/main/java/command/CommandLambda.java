package command;

/**
 * Created by Emil on 06/12/2016.
 */
public interface CommandLambda {
    public void execute();
}
