package command;

/**
 * Created by Emil on 04/12/2016.
 */
public interface Command {
    public void execute();
    public void undo();
}
