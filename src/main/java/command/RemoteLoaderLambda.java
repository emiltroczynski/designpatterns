package command;

/**
 * Created by Emil on 06/12/2016.
 */
public class RemoteLoaderLambda {
    public static void main(String[] args) {
        RemoteControlLambda remoteControlLambda = new RemoteControlLambda();

        LightLambda livingRoomLight = new LightLambda("Living Room");
        StereoLambda stereoLambda = new StereoLambda("Living Room");

        remoteControlLambda.setCommand(0, livingRoomLight::on, livingRoomLight::off);

        CommandLambda stereoWithCD = () -> { stereoLambda.on(); stereoLambda.setCD();
            stereoLambda.setVolume(11);
        };

        remoteControlLambda.setCommand(1, stereoWithCD, stereoLambda::off);

        System.out.println(remoteControlLambda);
        remoteControlLambda.onButtonWasPushed(0);
        remoteControlLambda.offButtonWasPushed(0);

        remoteControlLambda.onButtonWasPushed(1);
        remoteControlLambda.offButtonWasPushed(1);
    }
}
