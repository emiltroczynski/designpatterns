package command;

/**
 * Created by Emil on 06/12/2016.
 */
public class LightLambda {
    String name;

    public LightLambda() {
    }

    public LightLambda(String name) {
        this.name = name;
    }

    public void on() {
        System.out.println(name + " light is on");
    }

    public void off() {
        System.out.println(name + " light is off");
    }
}
