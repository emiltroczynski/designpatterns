package command;

/**
 * Created by Emil on 04/12/2016.
 */
public class Stereo {
    String name;

    public Stereo(String name) {
        this.name = name;
    }

    public void on() {
        System.out.println("Stereo is on");
    }

    public void off() {
        System.out.println("Stereo is off");
    }

    public void setCD() {
        System.out.println("Stereo is on and CD is playing");
    }

    public void setVolume(int volume) {
        System.out.println("Volume is set to: " + volume);
    }
}
