package command;

/**
 * Created by Emil on 04/12/2016.
 */
public class GarageDoor {
    String name;

    public GarageDoor() {
    }

    public GarageDoor(String name) {
        this.name = name;
    }

    public void up() {
        System.out.println("Door is open");
    }

    public void down() {
        System.out.println("Door is close");
    }

    public void stop() {
        System.out.println("Door is stop");
    }

    public void lightOn() {
        System.out.println("Door light is on");
    }

    public void lightOff() {
        System.out.println("Door light is off");
    }
}
