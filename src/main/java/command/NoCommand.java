package command;

/**
 * Created by Emil on 04/12/2016.
 */
public class NoCommand implements Command {
    @Override
    public void execute() {

    }

    @Override
    public void undo() {

    }
}
