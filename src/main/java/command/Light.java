package command;

/**
 * Created by Emil on 04/12/2016.
 */
public class Light {
    String name;

    public Light() {
    }

    public Light(String name) {
        this.name = name;
    }

    public void on() {
        System.out.println(name + " light is on");
    }

    public void off() {
        System.out.println(name + " light is off");
    }
}
