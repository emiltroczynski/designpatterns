package command;

/**
 * Created by Emil on 04/12/2016.
 */
public class RemoteLoader {
    public static void main(String[] args) {
        RemoteControl remoteControl = new RemoteControl();

        Light livingRoomLight = new Light("Living Room");
        Light kitchenLight = new Light("Kitchen");
        GarageDoor garageDoor = new GarageDoor("");
        Stereo stereo = new Stereo("Living room");

        LightOnCommand livingRoomLightOn = new LightOnCommand(livingRoomLight);
        LightOffCommand livingRoomLightOff = new LightOffCommand(livingRoomLight);

        LightOnCommand kitchenRoomLightOn = new LightOnCommand(kitchenLight);
        LightOffCommand kitchenRoomLightOff = new LightOffCommand(kitchenLight);

        GarageDoorOpenCommand garageDoorUp = new GarageDoorOpenCommand(garageDoor);
        GarageDoorCloseCommand garageDoorDown = new GarageDoorCloseCommand(garageDoor);

        StereoOnWithCDCommand stereoOnWithCDCommand = new StereoOnWithCDCommand(stereo);
        StereoOffCommand stereoOffCommand = new StereoOffCommand(stereo);

        remoteControl.setCommand(0, livingRoomLightOn, livingRoomLightOff);
        remoteControl.setCommand(1, kitchenRoomLightOn, kitchenRoomLightOff);
        remoteControl.setCommand(2, garageDoorUp, garageDoorDown);
        remoteControl.setCommand(3, stereoOnWithCDCommand, stereoOffCommand);


        System.out.println(remoteControl);

        remoteControl.onButtonWasPushed(0);
        remoteControl.offButtonWasPushed(0);

        remoteControl.onButtonWasPushed(1);
        remoteControl.offButtonWasPushed(1);

        remoteControl.onButtonWasPushed(2);
        remoteControl.offButtonWasPushed(2);

        remoteControl.onButtonWasPushed(3);
        remoteControl.offButtonWasPushed(3);


        System.out.println("--- Undo ---");
        RemoteControlWithUndo remoteControlWithUndo = new RemoteControlWithUndo();

        Light livingRoomLight2 = new Light("Living Room");

        LightOnCommand lightOnCommand2 = new LightOnCommand(livingRoomLight2);
        LightOffCommand lightOffCommand2 = new LightOffCommand(livingRoomLight2);
        remoteControlWithUndo.setCommand(0, lightOnCommand2, lightOffCommand2);

        remoteControlWithUndo.onButtonWasPushed(0);
        remoteControlWithUndo.offButtonWasPushed(0);
        System.out.println(remoteControlWithUndo);
        remoteControlWithUndo.undoButtonWasPushed();
        remoteControlWithUndo.offButtonWasPushed(0);
        remoteControlWithUndo.onButtonWasPushed(0);
        System.out.println(remoteControlWithUndo);
        remoteControlWithUndo.undoButtonWasPushed();

        System.out.println("\n------- Undo + state -------");
        RemoteControlWithUndo remoteControlState = new RemoteControlWithUndo();
        CeilingFan ceilingFan2 = new CeilingFan("Living Room");

        CeilingFanMediumCommand ceilingFanMedium = new CeilingFanMediumCommand(ceilingFan2);
        CeilingFanHighCommand ceilingFanHigh = new CeilingFanHighCommand(ceilingFan2);
        CeilingFanOffCommand ceilingFanOff = new CeilingFanOffCommand(ceilingFan2);

        remoteControlState.setCommand(0, ceilingFanMedium, ceilingFanOff);
        remoteControlState.setCommand(1, ceilingFanHigh, ceilingFanOff);

        remoteControlState.onButtonWasPushed(0);
        remoteControlState.offButtonWasPushed(0);
        System.out.println(remoteControlState);
        remoteControlState.undoButtonWasPushed();

        remoteControlState.onButtonWasPushed(1);
        System.out.println(remoteControlState);
        remoteControlState.undoButtonWasPushed();


        System.out.println("\n------- Macro -------");
        RemoteControlWithUndo remoteControlParty = new RemoteControlWithUndo();
        Light light = new Light("Living Room");
        Stereo stereo2 = new Stereo("Living Room");
        CeilingFan ceilingFan3 = new CeilingFan("Living Room");

        LightOnCommand lightOn = new LightOnCommand(light);
        StereoOnWithCDCommand stereoOnWithCDCommand2 = new StereoOnWithCDCommand(stereo2);
        CeilingFanHighCommand ceilingFan4 = new CeilingFanHighCommand(ceilingFan3);
        Command[] partyOn = {lightOn, stereoOnWithCDCommand2, ceilingFan4};
        MacroCommand partyOnMacro = new MacroCommand(partyOn);
        remoteControlParty.setCommand(0, partyOnMacro, new NoCommand());

        System.out.println(remoteControlParty);
        remoteControlParty.onButtonWasPushed(0);
        remoteControlParty.undoButtonWasPushed();
    }
}
