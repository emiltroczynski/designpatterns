package iterator;

/**
 * Created by Emil_Troczynski on 11/12/2016.
 */
public interface Iterator {
    boolean hasNext();
    Object next();
}
