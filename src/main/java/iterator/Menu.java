package iterator;
import java.util.Iterator;
/**
 * Created by Emil_Troczynski on 12/12/2016.
 */
public interface Menu {
    public Iterator<MenuItem> createIterator();
}