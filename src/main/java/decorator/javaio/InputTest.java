package decorator.javaio;

import javax.imageio.ImageIO;
import java.io.*;

/**
 * Created by Emil on 27/11/2016.
 */
public class InputTest {
    public static void main(String[] args) {
        int c;

        try {
            File file = new File(ImageIO.getCacheDirectory(), "te.txt");
            InputStream in = new LowerCaseInputStream(new BufferedInputStream(new FileInputStream(".\\src\\main\\java\\decorator\\javaio\\InputTest.txt")));

            while ((c = in.read()) >= 0){
                System.out.print((char) c);
            }
            in.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
