package decorator;

/**
 * Created by Emil on 27/11/2016.
 */
public abstract class CondimentDecorator extends Beverage {
    public abstract String getDescription();
}
