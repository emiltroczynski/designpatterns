package mvc;

/**
 * Created by Emil_Troczynski on 02/01/2017.
 */
public interface BeatModelInterface {
    void initialize();

    void on();

    void off();

    int getBPM();

    void setBPM(int bpm);

    void registerObserver(BeatObserver o);

    void removeObserver(BeatObserver o);

    void registerObserver(BPMObserver o);

    void removeObserver(BPMObserver o);
}
