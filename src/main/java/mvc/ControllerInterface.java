package mvc;

/**
 * Created by Emil_Troczynski on 03/01/2017.
 */
public interface ControllerInterface {
    void start();
    void stop();
    void increaseBPM();
    void descreaseBPM();
    void setBPM(int bpm);
}
