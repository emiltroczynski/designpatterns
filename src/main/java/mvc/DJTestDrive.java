package mvc;

/**
 * Created by Emil_Troczynski on 03/01/2017.
 */
public class DJTestDrive {
    public static void main(String[] args) {
        BeatModelInterface model = new BeatModel();
        ControllerInterface controller = new BeatController(model);
    }
}
