package mvc;

/**
 * Created by Emil_Troczynski on 02/01/2017.
 */
public interface BPMObserver {
    void updateBPM();
}
