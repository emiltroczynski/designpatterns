package state;

/**
 * Created by Emil_Troczynski on 15/12/2016.
 */
public class GumballMachineTestDriveUpdated {
    public static void main(String[] args) {
        GumballMachineUpdated gumballMachine = new GumballMachineUpdated(5);
        System.out.println(gumballMachine);

        gumballMachine.insertQuarter();
        gumballMachine.turnCrank();

        System.out.println(gumballMachine);

        gumballMachine.insertQuarter();
        gumballMachine.turnCrank();

        gumballMachine.insertQuarter();
        gumballMachine.turnCrank();

        System.out.println(gumballMachine);
    }
}
