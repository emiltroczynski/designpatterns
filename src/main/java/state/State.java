package state;

/**
 * Created by Emil_Troczynski on 15/12/2016.
 */
public interface State {
    public void insertQuarter();
    public void ejectQuarter();
    public void turnCrank();
    public void dispense();
}
