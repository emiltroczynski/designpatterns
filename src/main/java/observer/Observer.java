package observer;

/**
 * Created by Emil on 23/11/2016.
 */
public interface Observer {
    public void update(float temp, float humidity, float pressure);
}
