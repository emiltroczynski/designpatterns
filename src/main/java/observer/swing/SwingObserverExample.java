package observer.swing;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

/**
 * Created by Emil on 26/11/2016.
 */
public class SwingObserverExample {
    JFrame frame;

    public static void main(String[] args) {
        SwingObserverExample example = new SwingObserverExample();
        example.go();
    }

    private void go() {
        frame = new JFrame();
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

        JButton button = new JButton("Should I do it?");
        button.addActionListener(new AngelListener());
        button.addActionListener(event ->
                System.out.println("Come on, do it!"));

        frame.getContentPane().add(button);
        frame.pack();
        frame.setSize(200,200);
        frame.setLocationByPlatform(true);
        frame.setVisible(true);
    }

    private class AngelListener implements ActionListener {

        public void actionPerformed(ActionEvent e) {
            System.out.println("Don't do it, you might regret it!");
        }
    }
}
