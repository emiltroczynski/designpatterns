package observer;

/**
 * Created by Emil on 23/11/2016.
 */
public interface DisplayElement {
    public void display();
}
