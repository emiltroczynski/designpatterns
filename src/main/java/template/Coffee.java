package template;

/**
 * Created by Emil_Troczynski on 12/8/2016.
 */
public class Coffee extends CaffeineBeverage {

    public void addCondiments() {
        System.out.println("Adding Sugar and Milk");
    }

    void brew() {
        System.out.println("Dripping Coffee through filter");
    }
}
