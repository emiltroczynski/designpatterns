package template;

/**
 * Created by Emil_Troczynski on 12/8/2016.
 */
public class Tea extends CaffeineBeverage implements Comparable {

    void addCondiments() {
        System.out.println("Adding Lemon");
    }

    void brew() {
        System.out.println("Steeping the tea");
    }

    @Override
    public int compareTo(Object o) {
        return 0;
    }
}
