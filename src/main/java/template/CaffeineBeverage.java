package template;

/**
 * Created by Emil_Troczynski on 12/8/2016.
 */
public abstract class CaffeineBeverage {
    final void prepareRecipe() {
        boilWater();
        brew();
        pourInCup();
        if (customerWantsCondiments()) {
            addCondiments();
        }
    }

    boolean customerWantsCondiments() {
        return true;
    }

    abstract void addCondiments();

    private void pourInCup() {
        System.out.println("Pouring into cup");
    }

    abstract void brew();

    private void boilWater() {
        System.out.println("Boiling water");
    }
}
