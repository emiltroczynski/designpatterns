package template;

/**
 * Created by Emil_Troczynski on 12/8/2016.
 */
public class BeverageTestDrive {
    public static void main(String[] args) {
        Tea tea = new Tea();
        CoffeeWithHook coffeeWithHook = new CoffeeWithHook();
        Coffee coffee = new Coffee();
        System.out.println("\nTea");
        tea.prepareRecipe();
        System.out.println("\nCoffee with hook");
        coffeeWithHook.prepareRecipe();
        System.out.println("\nCoffee without hook");
        coffee.prepareRecipe();

    }
}
