package facade;

/**
 * Created by Emil on 06/12/2016.
 */
public class Screen {
    public void down() {
        System.out.println("Theater Screen going down");
    }
}
