package facade;

/**
 * Created by Emil on 06/12/2016.
 */
public class Projector {
    public void on() {
        System.out.println("Top-O-Line Projector on");
    }

    public void wideScreenMode() {
        System.out.println("Top-O-Line Projector oin widescreen mode");
    }
}
