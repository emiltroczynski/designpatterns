package facade;

/**
 * Created by Emil on 06/12/2016.
 */
public class PopcornPopper {
    public void on() {
        System.out.println("Popcorn popper on");
    }

    public void pop() {
        System.out.println("Popcorn Popper popping popcorn");
    }
}
