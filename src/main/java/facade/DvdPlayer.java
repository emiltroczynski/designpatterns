package facade;

/**
 * Created by Emil on 06/12/2016.
 */
public class DvdPlayer {
    public void on() {
        System.out.println("DVD on");
    }

    public void play(String movie) {
        System.out.println("DVD is playing: " + movie);
    }
}
