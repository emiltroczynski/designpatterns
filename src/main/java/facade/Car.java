package facade;

/**
 * Created by Emil_Troczynski on 12/7/2016.
 */
public class Car {
    Engine engine;
    //other instance variables

    public Car() {

    }

    public void start(Key key) {
        Doors doors = new Doors();

        boolean authorized = key.turns();

        if (authorized) {
            engine.start();
            updateDashboardDisplay();
            doors.lock();
        }

    }

    private void updateDashboardDisplay() {
        //update display
    }

}
