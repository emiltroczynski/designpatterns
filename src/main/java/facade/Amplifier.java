package facade;

/**
 * Created by Emil on 06/12/2016.
 */
public class Amplifier {
    public void on() {
        System.out.println("Top-O-Line Amplifier on");
    }

    public void setDvd(DvdPlayer dvd) {
        System.out.println("Top-O-Line Amplifier setting DVD player");
    }

    public void setSurrondSound() {
        System.out.println("Top-O-Line Amplifier surround on");
    }

    public void setVolume(int volume) {
        System.out.println("Top-O-Line Amplifier setting volume to " + volume);
    }
}
