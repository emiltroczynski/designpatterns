package facade;

/**
 * Created by Emil on 06/12/2016.
 */
public class TheaterLights {
    public void dim(int dimming) {
        System.out.println("Theater Ceiling Lights dimming to " + dimming + "%");
    }
}
